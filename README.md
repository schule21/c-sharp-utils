# C-Sharp utils

C# utilities for some of School 21 projects

## C#-calculator

Applies an arithmetic operation (+, -, *, /, %) to two numbers passed in the command line.

## How to use

### Compile 

```mcs calc.cs```

or

```make calc```


### Run the executable with three parameters

```mono ./calc.exe <argument 1> <operation> <argument 1> ```

For example,

```mono ./calc.exe 2 + 3 ```

or

```mono ./calc.exe 12345678.9012345 '*' 0.1 ```

*Note* Multiplication sign must be in single quotes to avoid shell globbing



