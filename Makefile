CALC_EXE=./calc.exe 
RUN_CALC=mono $(CALC_EXE)

calc: calc.cs
	mcs calc.cs
	$(RUN_CALC)

	$(RUN_CALC) 20 + 2
	$(RUN_CALC) 15 - 7
	$(RUN_CALC) 7 '*' 8
	$(RUN_CALC) 2 / 3

	$(RUN_CALC) 0.1 + 0.2
	$(RUN_CALC) 123456789 '*' 7

	$(RUN_CALC) 2 '%' 3
	$(RUN_CALC) -2 '%' 3
	$(RUN_CALC) 2 '%' -3
	$(RUN_CALC) -2 '%' -3


clean:
	rm -rf $(CALC_EXE)
