using System;
using System.Globalization;

class TestClass
{

    const bool Verbose = true;

    public static void showDecimal(decimal value)
    {
        int[] bits = decimal.GetBits(value);
        Console.WriteLine("{0,31}  {1,10:X8}{2,10:X8}{3,10:X8}{4,10:X8}",
                          value, bits[3], bits[2], bits[1], bits[0]);
    }

    public static decimal bin_op(decimal a, string op, decimal b)
    {
        switch (op) {
                case "+":
                    return a + b;
                case "-":
                    return a - b;
                case "*":
                    return a * b;
                case "/":
                    return a / b;
                case "%":
                    return a % b;
                default:
                    Console.WriteLine("Invalid op {0}.", op);
                    return 0;
            }
    }

    static void Main(string[] args)
    {
        if (args.Length != 3) {
            Console.WriteLine("Usage {0} ARG1 OP ARG2", System.AppDomain.CurrentDomain.FriendlyName);
               System.Environment.Exit(0);
        }

        NumberStyles style =  NumberStyles.Number;
        CultureInfo provider = CultureInfo.InvariantCulture;

        decimal arg1 = Decimal.Parse(args[0], style, provider) ;
        string op = args[1];
        decimal arg2 = Decimal.Parse(args[2], style, provider);

        // Optional header
        if (Verbose)
        {
            Console.WriteLine("{0,31}  {1,10:X8}{2,10:X8}{3,10:X8}{4,10:X8}",
                        "Argument", "Bits[3]", "Bits[2]", "Bits[1]",
                        "Bits[0]" );
            Console.WriteLine( "{0,31}  {1,10:X8}{2,10:X8}{3,10:X8}{4,10:X8}",
                         "--------", "-------", "-------", "-------",
                         "-------" );
        }
        showDecimal(arg1);
        showDecimal(arg2);
        decimal res = bin_op(arg1, op, arg2);
        Console.WriteLine("{0} {1} {2}", arg1.ToString(), op, arg2.ToString());
        showDecimal(res);
    }
}
